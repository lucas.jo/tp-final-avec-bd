document.addEventListener("DOMContentLoaded", function (event) {
    // BOUTON POUR RETOURNER VERS LE HAUT
    document.querySelector('#retour').addEventListener('click', fScroll);

    // SCRIPT POUR L'ANIMATION DE L'ÉCRAN DE CHARGEMENT, EN JQUERY (ce n'est pas écrit interdit au Jquery dans le document)
    $(window).on("load",function () {
        $(".loader-wrapper").fadeOut("slow");
    });

    function fScroll(e) {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    let connexion = new MovieDb();

    if (document.location.pathname.search("fiche-film.html") > 0) {
        var parametres = (new URL(document.location)).searchParams;

        connexion.requeteInfosFilm(parametres.get("id"));
        connexion.requeteInfoActeurs(parametres.get("id"));

    } else {
        connexion.requeteFilmsPopulaire();
        connexion.requeteFilmsNowPlaying();
    }
});

class MovieDb {

    constructor() {
        this.APIKey = "fe801af510d7299cca253f3eec57b146";
        this.lang = "fr-CA";
        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";
        this.posterSizes = ["92", "154", "185", "342", "500", "780"];
        this.tailleActeurs = ["45", "185"];

        this.movieTotal = 6;
        this.swiperTotal = 9;
        this.totalActeur = 6;
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS DU MOMENT
    requeteFilmsNowPlaying() {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteNowPlaying.bind(this));
        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS DU MOMENT
    retourRequeteNowPlaying(e) {
        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficherNowPlaying(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS DU MOMENT
    afficherNowPlaying(data) {

        for (let i = 0; i < this.swiperTotal; i++) {
            let unFilmCarrousel = document.querySelector(".template>div.swiper-slide").cloneNode(true);
            unFilmCarrousel.querySelector("h3").innerText = data[i].title;
            unFilmCarrousel.querySelector("p").innerText = "Noté " + data[i].vote_average + " sur 10";

            let uneImage = unFilmCarrousel.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[3] + data[i].poster_path);
            unFilmCarrousel.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector("div.swiper-wrapper").appendChild(unFilmCarrousel);
        }
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoHeight: true,
            autoplay: { delay: 6000, },

            centeredSlides: 'true',

            breakpoints: {
                320: {
                    slidesPerView: 2,
                    spaceBetween: 50
                },


                768: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },

                1000: {
                    slidesPerView: 4,
                    spaceBetween: 20
                },

                1920: {
                    slidesPerView: 6,
                    spaceBetween: 20
                }
            },

            pagination: {
                el: '.swiper-pagination',
            },

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            scrollbar: {
                hide: '.swiper-scrollbar',
                draggable: true,
                dragSize: 'auto',
            },
        })
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS POPULAIRES
    requeteFilmsPopulaire() {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteFilmsPopulaire.bind(this));
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS POPULAIRES
    retourRequeteFilmsPopulaire(e) {
        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficherFilmsPopulaire(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS POPULAIRES
    afficherFilmsPopulaire(data) {
        for (let i = 0; i < this.movieTotal; i++) {
            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".cote-film").innerText = data[i].vote_average;
            unArticle.querySelector(".dateSortie").innerText = data[i].release_date;

            if (data[i].overview === "") {
                unArticle.querySelector(".description").innerText = "Ce film n'a pas de description en langue française.";
            } else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[5] + data[i].poster_path);
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector("section.liste-films").appendChild(unArticle);
        }
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS
    requeteInfosFilm(idFilm) {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfosFilm.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS
    retourRequeteInfosFilm(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText);
            this.afficherInfosFilm(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS
    afficherInfosFilm(data) {
        document.querySelector("img.affiche").setAttribute("src", this.imgPath + "w" + this.posterSizes[4] + data.poster_path);
        document.querySelector("h1").innerText = data.title;
        document.querySelector("p.anneeParution").innerText = data.release_date;
        document.querySelector("p.cote-film").innerText = data.vote_average + "/10";
        document.querySelector("p.langue-originale").innerText = data.original_language;
        document.querySelector("p.duree").innerText = data.runtime + " minutes";

        if (data.budget == "0") {
            document.querySelector("p.budget").innerText = "Non public";
        } else {
            document.querySelector("p.budget").innerText = " USD$ " + data.budget;
        }

        if (data.revenue == "0") {
            document.querySelector("p.recette").innerText = "Non public";
        } else {
            document.querySelector(".description").innerText = " USD$ " + data.revenue;
        }

        if (data.overview == "") {
            document.querySelector(".description").innerText = "Ce film n'a pas de description en langue française.";
        } else {
            document.querySelector(".description").innerText = data.overview;
        }
    }

    // RÉCUPÉRATION DE L'INFORMATION DES ACTEURS
    requeteInfoActeurs(idFilm) {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfosActeurs.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES ACTEURS
    retourRequeteInfosActeurs(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).cast;
            this.afficherInfosActeurs(data);
        }
    }

    // AFFICHAGE DE L'INFORMATION DES ACTEURS
    afficherInfosActeurs(data) {
        for (let i = 0; i < this.totalActeur; i++) {
            let unActeur = document.querySelector(".template>article.acteur").cloneNode(true);
            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w" + this.tailleActeurs[1] + data[i].profile_path);
            unActeur.querySelector("h5").innerText = data[i].name;

            document.querySelector(".liste-acteurs").appendChild(unActeur);
        }
    }


}