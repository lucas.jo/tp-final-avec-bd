document.addEventListener("DOMContentLoaded", function (event) {
    // BOUTON POUR RETOURNER VERS LE HAUT
    document.querySelector('#retour').addEventListener('click', fScroll);

    // SCRIPT POUR L'ANIMATION DE L'ÉCRAN DE CHARGEMENT, EN JQUERY (ce n'est pas écrit interdit au Jquery dans le document)
    $(window).on("load",function () {
        $(".loader-wrapper").fadeOut("slow");
    });

    function fScroll(e) {
        window.scroll({
            top: 0,
            left: 0,
            behavior: 'smooth'
        });
    }

    let connexion = new MovieDb();

    if (document.location.pathname.search("fiche-film.html") > 0) {
        var parametres = (new URL(document.location)).searchParams;

        connexion.requeteInfosFilm(parametres.get("id"));
        connexion.requeteInfoActeurs(parametres.get("id"));

    } else {
        connexion.requeteFilmsPopulaire();
        connexion.requeteFilmsNowPlaying();
    }
});

class MovieDb {

    constructor() {
        this.APIKey = "fe801af510d7299cca253f3eec57b146";
        this.lang = "fr-CA";
        this.baseURL = "https://api.themoviedb.org/3/";

        this.imgPath = "http://image.tmdb.org/t/p/";
        this.posterSizes = ["92", "154", "185", "342", "500", "780"];
        this.tailleActeurs = ["45", "185"];

        this.movieTotal = 6;
        this.swiperTotal = 9;
        this.totalActeur = 6;
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS DU MOMENT
    requeteFilmsNowPlaying() {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteNowPlaying.bind(this));
        xhr.open("GET", this.baseURL + "movie/now_playing?page=1&language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS DU MOMENT
    retourRequeteNowPlaying(e) {
        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficherNowPlaying(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS DU MOMENT
    afficherNowPlaying(data) {

        for (let i = 0; i < this.swiperTotal; i++) {
            let unFilmCarrousel = document.querySelector(".template>div.swiper-slide").cloneNode(true);
            unFilmCarrousel.querySelector("h3").innerText = data[i].title;
            unFilmCarrousel.querySelector("p").innerText = "Noté " + data[i].vote_average + " sur 10";

            let uneImage = unFilmCarrousel.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[3] + data[i].poster_path);
            unFilmCarrousel.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector("div.swiper-wrapper").appendChild(unFilmCarrousel);
        }
        var mySwiper = new Swiper('.swiper-container', {
            // Optional parameters
            direction: 'horizontal',
            loop: true,
            autoHeight: true,
            autoplay: { delay: 6000, },

            centeredSlides: 'true',

            breakpoints: {
                320: {
                    slidesPerView: 2,
                    spaceBetween: 50
                },


                768: {
                    slidesPerView: 2,
                    spaceBetween: 10
                },

                1000: {
                    slidesPerView: 4,
                    spaceBetween: 20
                },

                1920: {
                    slidesPerView: 6,
                    spaceBetween: 20
                }
            },

            pagination: {
                el: '.swiper-pagination',
            },

            navigation: {
                nextEl: '.swiper-button-next',
                prevEl: '.swiper-button-prev',
            },

            scrollbar: {
                hide: '.swiper-scrollbar',
                draggable: true,
                dragSize: 'auto',
            },
        })
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS POPULAIRES
    requeteFilmsPopulaire() {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteFilmsPopulaire.bind(this));
        xhr.open("GET", this.baseURL + "movie/popular?page=1&language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS POPULAIRES
    retourRequeteFilmsPopulaire(e) {
        let target = e.currentTarget; //XMLHttpRequest
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).results;
            this.afficherFilmsPopulaire(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS POPULAIRES
    afficherFilmsPopulaire(data) {
        for (let i = 0; i < this.movieTotal; i++) {
            let unArticle = document.querySelector(".template>article.film").cloneNode(true);

            unArticle.querySelector("h2").innerText = data[i].title;
            unArticle.querySelector(".cote-film").innerText = data[i].vote_average;
            unArticle.querySelector(".dateSortie").innerText = data[i].release_date;

            if (data[i].overview === "") {
                unArticle.querySelector(".description").innerText = "Ce film n'a pas de description en langue française.";
            } else {
                unArticle.querySelector(".description").innerText = data[i].overview;
            }

            let uneImage = unArticle.querySelector("img");
            uneImage.setAttribute("src", this.imgPath + "w" + this.posterSizes[5] + data[i].poster_path);
            unArticle.querySelector("a").setAttribute("href", "fiche-film.html?id=" + data[i].id);

            document.querySelector("section.liste-films").appendChild(unArticle);
        }
    }

    // RÉCUPÉRATION DE L'INFORMATION DES FILMS
    requeteInfosFilm(idFilm) {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfosFilm.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + idFilm + "?language=" + this.lang + "&api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES FILMS
    retourRequeteInfosFilm(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText);
            this.afficherInfosFilm(data);
        }
    }

    // AFFICHAGE DES INFORMATIONS DES FILMS
    afficherInfosFilm(data) {
        document.querySelector("img.affiche").setAttribute("src", this.imgPath + "w" + this.posterSizes[4] + data.poster_path);
        document.querySelector("h1").innerText = data.title;
        document.querySelector("p.anneeParution").innerText = data.release_date;
        document.querySelector("p.cote-film").innerText = data.vote_average + "/10";
        document.querySelector("p.langue-originale").innerText = data.original_language;
        document.querySelector("p.duree").innerText = data.runtime + " minutes";

        if (data.budget == "0") {
            document.querySelector("p.budget").innerText = "Non public";
        } else {
            document.querySelector("p.budget").innerText = " USD$ " + data.budget;
        }

        if (data.revenue == "0") {
            document.querySelector("p.recette").innerText = "Non public";
        } else {
            document.querySelector(".description").innerText = " USD$ " + data.revenue;
        }

        if (data.overview == "") {
            document.querySelector(".description").innerText = "Ce film n'a pas de description en langue française.";
        } else {
            document.querySelector(".description").innerText = data.overview;
        }
    }

    // RÉCUPÉRATION DE L'INFORMATION DES ACTEURS
    requeteInfoActeurs(idFilm) {
        var xhr = new XMLHttpRequest();

        xhr.addEventListener("readystatechange", this.retourRequeteInfosActeurs.bind(this));
        xhr.open("GET", this.baseURL + "movie/" + idFilm + "/credits?api_key=" + this.APIKey);
        xhr.send();
    }

    // RETOUR DE LA REQUÊTE POUR L'INFORMATION DES ACTEURS
    retourRequeteInfosActeurs(e) {
        let target = e.currentTarget;
        let data;

        if (target.readyState === target.DONE) {
            data = JSON.parse(target.responseText).cast;
            this.afficherInfosActeurs(data);
        }
    }

    // AFFICHAGE DE L'INFORMATION DES ACTEURS
    afficherInfosActeurs(data) {
        for (let i = 0; i < this.totalActeur; i++) {
            let unActeur = document.querySelector(".template>article.acteur").cloneNode(true);
            unActeur.querySelector("img").setAttribute("src", this.imgPath + "w" + this.tailleActeurs[1] + data[i].profile_path);
            unActeur.querySelector("h5").innerText = data[i].name;

            document.querySelector(".liste-acteurs").appendChild(unActeur);
        }
    }


}
//# sourceMappingURL=data:application/json;charset=utf8;base64,eyJ2ZXJzaW9uIjozLCJuYW1lcyI6W10sIm1hcHBpbmdzIjoiIiwic291cmNlcyI6WyJzY3JpcHQuanMiXSwic291cmNlc0NvbnRlbnQiOlsiZG9jdW1lbnQuYWRkRXZlbnRMaXN0ZW5lcihcIkRPTUNvbnRlbnRMb2FkZWRcIiwgZnVuY3Rpb24gKGV2ZW50KSB7XG4gICAgLy8gQk9VVE9OIFBPVVIgUkVUT1VSTkVSIFZFUlMgTEUgSEFVVFxuICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoJyNyZXRvdXInKS5hZGRFdmVudExpc3RlbmVyKCdjbGljaycsIGZTY3JvbGwpO1xuXG4gICAgLy8gU0NSSVBUIFBPVVIgTCdBTklNQVRJT04gREUgTCfDiUNSQU4gREUgQ0hBUkdFTUVOVCwgRU4gSlFVRVJZIChjZSBuJ2VzdCBwYXMgw6ljcml0IGludGVyZGl0IGF1IEpxdWVyeSBkYW5zIGxlIGRvY3VtZW50KVxuICAgICQod2luZG93KS5vbihcImxvYWRcIixmdW5jdGlvbiAoKSB7XG4gICAgICAgICQoXCIubG9hZGVyLXdyYXBwZXJcIikuZmFkZU91dChcInNsb3dcIik7XG4gICAgfSk7XG5cbiAgICBmdW5jdGlvbiBmU2Nyb2xsKGUpIHtcbiAgICAgICAgd2luZG93LnNjcm9sbCh7XG4gICAgICAgICAgICB0b3A6IDAsXG4gICAgICAgICAgICBsZWZ0OiAwLFxuICAgICAgICAgICAgYmVoYXZpb3I6ICdzbW9vdGgnXG4gICAgICAgIH0pO1xuICAgIH1cblxuICAgIGxldCBjb25uZXhpb24gPSBuZXcgTW92aWVEYigpO1xuXG4gICAgaWYgKGRvY3VtZW50LmxvY2F0aW9uLnBhdGhuYW1lLnNlYXJjaChcImZpY2hlLWZpbG0uaHRtbFwiKSA+IDApIHtcbiAgICAgICAgdmFyIHBhcmFtZXRyZXMgPSAobmV3IFVSTChkb2N1bWVudC5sb2NhdGlvbikpLnNlYXJjaFBhcmFtcztcblxuICAgICAgICBjb25uZXhpb24ucmVxdWV0ZUluZm9zRmlsbShwYXJhbWV0cmVzLmdldChcImlkXCIpKTtcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVJbmZvQWN0ZXVycyhwYXJhbWV0cmVzLmdldChcImlkXCIpKTtcblxuICAgIH0gZWxzZSB7XG4gICAgICAgIGNvbm5leGlvbi5yZXF1ZXRlRmlsbXNQb3B1bGFpcmUoKTtcbiAgICAgICAgY29ubmV4aW9uLnJlcXVldGVGaWxtc05vd1BsYXlpbmcoKTtcbiAgICB9XG59KTtcblxuY2xhc3MgTW92aWVEYiB7XG5cbiAgICBjb25zdHJ1Y3RvcigpIHtcbiAgICAgICAgdGhpcy5BUElLZXkgPSBcImZlODAxYWY1MTBkNzI5OWNjYTI1M2YzZWVjNTdiMTQ2XCI7XG4gICAgICAgIHRoaXMubGFuZyA9IFwiZnItQ0FcIjtcbiAgICAgICAgdGhpcy5iYXNlVVJMID0gXCJodHRwczovL2FwaS50aGVtb3ZpZWRiLm9yZy8zL1wiO1xuXG4gICAgICAgIHRoaXMuaW1nUGF0aCA9IFwiaHR0cDovL2ltYWdlLnRtZGIub3JnL3QvcC9cIjtcbiAgICAgICAgdGhpcy5wb3N0ZXJTaXplcyA9IFtcIjkyXCIsIFwiMTU0XCIsIFwiMTg1XCIsIFwiMzQyXCIsIFwiNTAwXCIsIFwiNzgwXCJdO1xuICAgICAgICB0aGlzLnRhaWxsZUFjdGV1cnMgPSBbXCI0NVwiLCBcIjE4NVwiXTtcblxuICAgICAgICB0aGlzLm1vdmllVG90YWwgPSA2O1xuICAgICAgICB0aGlzLnN3aXBlclRvdGFsID0gOTtcbiAgICAgICAgdGhpcy50b3RhbEFjdGV1ciA9IDY7XG4gICAgfVxuXG4gICAgLy8gUsOJQ1VQw4lSQVRJT04gREUgTCdJTkZPUk1BVElPTiBERVMgRklMTVMgRFUgTU9NRU5UXG4gICAgcmVxdWV0ZUZpbG1zTm93UGxheWluZygpIHtcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91clJlcXVldGVOb3dQbGF5aW5nLmJpbmQodGhpcykpO1xuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL25vd19wbGF5aW5nP3BhZ2U9MSZsYW5ndWFnZT1cIiArIHRoaXMubGFuZyArIFwiJmFwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XG4gICAgICAgIHhoci5zZW5kKCk7XG4gICAgfVxuXG4gICAgLy8gUkVUT1VSIERFIExBIFJFUVXDilRFIFBPVVIgTCdJTkZPUk1BVElPTiBERVMgRklMTVMgRFUgTU9NRU5UXG4gICAgcmV0b3VyUmVxdWV0ZU5vd1BsYXlpbmcoZSkge1xuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0OyAvL1hNTEh0dHBSZXF1ZXN0XG4gICAgICAgIGxldCBkYXRhO1xuXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XG4gICAgICAgICAgICB0aGlzLmFmZmljaGVyTm93UGxheWluZyhkYXRhKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIEFGRklDSEFHRSBERVMgSU5GT1JNQVRJT05TIERFUyBGSUxNUyBEVSBNT01FTlRcbiAgICBhZmZpY2hlck5vd1BsYXlpbmcoZGF0YSkge1xuXG4gICAgICAgIGZvciAobGV0IGkgPSAwOyBpIDwgdGhpcy5zd2lwZXJUb3RhbDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgdW5GaWxtQ2Fycm91c2VsID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5kaXYuc3dpcGVyLXNsaWRlXCIpLmNsb25lTm9kZSh0cnVlKTtcbiAgICAgICAgICAgIHVuRmlsbUNhcnJvdXNlbC5xdWVyeVNlbGVjdG9yKFwiaDNcIikuaW5uZXJUZXh0ID0gZGF0YVtpXS50aXRsZTtcbiAgICAgICAgICAgIHVuRmlsbUNhcnJvdXNlbC5xdWVyeVNlbGVjdG9yKFwicFwiKS5pbm5lclRleHQgPSBcIk5vdMOpIFwiICsgZGF0YVtpXS52b3RlX2F2ZXJhZ2UgKyBcIiBzdXIgMTBcIjtcblxuICAgICAgICAgICAgbGV0IHVuZUltYWdlID0gdW5GaWxtQ2Fycm91c2VsLnF1ZXJ5U2VsZWN0b3IoXCJpbWdcIik7XG4gICAgICAgICAgICB1bmVJbWFnZS5zZXRBdHRyaWJ1dGUoXCJzcmNcIiwgdGhpcy5pbWdQYXRoICsgXCJ3XCIgKyB0aGlzLnBvc3RlclNpemVzWzNdICsgZGF0YVtpXS5wb3N0ZXJfcGF0aCk7XG4gICAgICAgICAgICB1bkZpbG1DYXJyb3VzZWwucXVlcnlTZWxlY3RvcihcImFcIikuc2V0QXR0cmlidXRlKFwiaHJlZlwiLCBcImZpY2hlLWZpbG0uaHRtbD9pZD1cIiArIGRhdGFbaV0uaWQpO1xuXG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiZGl2LnN3aXBlci13cmFwcGVyXCIpLmFwcGVuZENoaWxkKHVuRmlsbUNhcnJvdXNlbCk7XG4gICAgICAgIH1cbiAgICAgICAgdmFyIG15U3dpcGVyID0gbmV3IFN3aXBlcignLnN3aXBlci1jb250YWluZXInLCB7XG4gICAgICAgICAgICAvLyBPcHRpb25hbCBwYXJhbWV0ZXJzXG4gICAgICAgICAgICBkaXJlY3Rpb246ICdob3Jpem9udGFsJyxcbiAgICAgICAgICAgIGxvb3A6IHRydWUsXG4gICAgICAgICAgICBhdXRvSGVpZ2h0OiB0cnVlLFxuICAgICAgICAgICAgYXV0b3BsYXk6IHsgZGVsYXk6IDYwMDAsIH0sXG5cbiAgICAgICAgICAgIGNlbnRlcmVkU2xpZGVzOiAndHJ1ZScsXG5cbiAgICAgICAgICAgIGJyZWFrcG9pbnRzOiB7XG4gICAgICAgICAgICAgICAgMzIwOiB7XG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1BlclZpZXc6IDIsXG4gICAgICAgICAgICAgICAgICAgIHNwYWNlQmV0d2VlbjogNTBcbiAgICAgICAgICAgICAgICB9LFxuXG5cbiAgICAgICAgICAgICAgICA3Njg6IHtcbiAgICAgICAgICAgICAgICAgICAgc2xpZGVzUGVyVmlldzogMixcbiAgICAgICAgICAgICAgICAgICAgc3BhY2VCZXR3ZWVuOiAxMFxuICAgICAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgICAgICAxMDAwOiB7XG4gICAgICAgICAgICAgICAgICAgIHNsaWRlc1BlclZpZXc6IDQsXG4gICAgICAgICAgICAgICAgICAgIHNwYWNlQmV0d2VlbjogMjBcbiAgICAgICAgICAgICAgICB9LFxuXG4gICAgICAgICAgICAgICAgMTkyMDoge1xuICAgICAgICAgICAgICAgICAgICBzbGlkZXNQZXJWaWV3OiA2LFxuICAgICAgICAgICAgICAgICAgICBzcGFjZUJldHdlZW46IDIwXG4gICAgICAgICAgICAgICAgfVxuICAgICAgICAgICAgfSxcblxuICAgICAgICAgICAgcGFnaW5hdGlvbjoge1xuICAgICAgICAgICAgICAgIGVsOiAnLnN3aXBlci1wYWdpbmF0aW9uJyxcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIG5hdmlnYXRpb246IHtcbiAgICAgICAgICAgICAgICBuZXh0RWw6ICcuc3dpcGVyLWJ1dHRvbi1uZXh0JyxcbiAgICAgICAgICAgICAgICBwcmV2RWw6ICcuc3dpcGVyLWJ1dHRvbi1wcmV2JyxcbiAgICAgICAgICAgIH0sXG5cbiAgICAgICAgICAgIHNjcm9sbGJhcjoge1xuICAgICAgICAgICAgICAgIGhpZGU6ICcuc3dpcGVyLXNjcm9sbGJhcicsXG4gICAgICAgICAgICAgICAgZHJhZ2dhYmxlOiB0cnVlLFxuICAgICAgICAgICAgICAgIGRyYWdTaXplOiAnYXV0bycsXG4gICAgICAgICAgICB9LFxuICAgICAgICB9KVxuICAgIH1cblxuICAgIC8vIFLDiUNVUMOJUkFUSU9OIERFIEwnSU5GT1JNQVRJT04gREVTIEZJTE1TIFBPUFVMQUlSRVNcbiAgICByZXF1ZXRlRmlsbXNQb3B1bGFpcmUoKSB7XG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJSZXF1ZXRlRmlsbXNQb3B1bGFpcmUuYmluZCh0aGlzKSk7XG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvcG9wdWxhcj9wYWdlPTEmbGFuZ3VhZ2U9XCIgKyB0aGlzLmxhbmcgKyBcIiZhcGlfa2V5PVwiICsgdGhpcy5BUElLZXkpO1xuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cblxuICAgIC8vIFJFVE9VUiBERSBMQSBSRVFVw4pURSBQT1VSIEwnSU5GT1JNQVRJT04gREVTIEZJTE1TIFBPUFVMQUlSRVNcbiAgICByZXRvdXJSZXF1ZXRlRmlsbXNQb3B1bGFpcmUoZSkge1xuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0OyAvL1hNTEh0dHBSZXF1ZXN0XG4gICAgICAgIGxldCBkYXRhO1xuXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpLnJlc3VsdHM7XG4gICAgICAgICAgICB0aGlzLmFmZmljaGVyRmlsbXNQb3B1bGFpcmUoZGF0YSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBBRkZJQ0hBR0UgREVTIElORk9STUFUSU9OUyBERVMgRklMTVMgUE9QVUxBSVJFU1xuICAgIGFmZmljaGVyRmlsbXNQb3B1bGFpcmUoZGF0YSkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMubW92aWVUb3RhbDsgaSsrKSB7XG4gICAgICAgICAgICBsZXQgdW5BcnRpY2xlID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5hcnRpY2xlLmZpbG1cIikuY2xvbmVOb2RlKHRydWUpO1xuXG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImgyXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0udGl0bGU7XG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5jb3RlLWZpbG1cIikuaW5uZXJUZXh0ID0gZGF0YVtpXS52b3RlX2F2ZXJhZ2U7XG4gICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kYXRlU29ydGllXCIpLmlubmVyVGV4dCA9IGRhdGFbaV0ucmVsZWFzZV9kYXRlO1xuXG4gICAgICAgICAgICBpZiAoZGF0YVtpXS5vdmVydmlldyA9PT0gXCJcIikge1xuICAgICAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiQ2UgZmlsbSBuJ2EgcGFzIGRlIGRlc2NyaXB0aW9uIGVuIGxhbmd1ZSBmcmFuw6dhaXNlLlwiO1xuICAgICAgICAgICAgfSBlbHNlIHtcbiAgICAgICAgICAgICAgICB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBkYXRhW2ldLm92ZXJ2aWV3O1xuICAgICAgICAgICAgfVxuXG4gICAgICAgICAgICBsZXQgdW5lSW1hZ2UgPSB1bkFydGljbGUucXVlcnlTZWxlY3RvcihcImltZ1wiKTtcbiAgICAgICAgICAgIHVuZUltYWdlLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMucG9zdGVyU2l6ZXNbNV0gKyBkYXRhW2ldLnBvc3Rlcl9wYXRoKTtcbiAgICAgICAgICAgIHVuQXJ0aWNsZS5xdWVyeVNlbGVjdG9yKFwiYVwiKS5zZXRBdHRyaWJ1dGUoXCJocmVmXCIsIFwiZmljaGUtZmlsbS5odG1sP2lkPVwiICsgZGF0YVtpXS5pZCk7XG5cbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJzZWN0aW9uLmxpc3RlLWZpbG1zXCIpLmFwcGVuZENoaWxkKHVuQXJ0aWNsZSk7XG4gICAgICAgIH1cbiAgICB9XG5cbiAgICAvLyBSw4lDVVDDiVJBVElPTiBERSBMJ0lORk9STUFUSU9OIERFUyBGSUxNU1xuICAgIHJlcXVldGVJbmZvc0ZpbG0oaWRGaWxtKSB7XG4gICAgICAgIHZhciB4aHIgPSBuZXcgWE1MSHR0cFJlcXVlc3QoKTtcblxuICAgICAgICB4aHIuYWRkRXZlbnRMaXN0ZW5lcihcInJlYWR5c3RhdGVjaGFuZ2VcIiwgdGhpcy5yZXRvdXJSZXF1ZXRlSW5mb3NGaWxtLmJpbmQodGhpcykpO1xuICAgICAgICB4aHIub3BlbihcIkdFVFwiLCB0aGlzLmJhc2VVUkwgKyBcIm1vdmllL1wiICsgaWRGaWxtICsgXCI/bGFuZ3VhZ2U9XCIgKyB0aGlzLmxhbmcgKyBcIiZhcGlfa2V5PVwiICsgdGhpcy5BUElLZXkpO1xuICAgICAgICB4aHIuc2VuZCgpO1xuICAgIH1cblxuICAgIC8vIFJFVE9VUiBERSBMQSBSRVFVw4pURSBQT1VSIEwnSU5GT1JNQVRJT04gREVTIEZJTE1TXG4gICAgcmV0b3VyUmVxdWV0ZUluZm9zRmlsbShlKSB7XG4gICAgICAgIGxldCB0YXJnZXQgPSBlLmN1cnJlbnRUYXJnZXQ7XG4gICAgICAgIGxldCBkYXRhO1xuXG4gICAgICAgIGlmICh0YXJnZXQucmVhZHlTdGF0ZSA9PT0gdGFyZ2V0LkRPTkUpIHtcbiAgICAgICAgICAgIGRhdGEgPSBKU09OLnBhcnNlKHRhcmdldC5yZXNwb25zZVRleHQpO1xuICAgICAgICAgICAgdGhpcy5hZmZpY2hlckluZm9zRmlsbShkYXRhKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIEFGRklDSEFHRSBERVMgSU5GT1JNQVRJT05TIERFUyBGSUxNU1xuICAgIGFmZmljaGVySW5mb3NGaWxtKGRhdGEpIHtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImltZy5hZmZpY2hlXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMucG9zdGVyU2l6ZXNbNF0gKyBkYXRhLnBvc3Rlcl9wYXRoKTtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcImgxXCIpLmlubmVyVGV4dCA9IGRhdGEudGl0bGU7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLmFubmVlUGFydXRpb25cIikuaW5uZXJUZXh0ID0gZGF0YS5yZWxlYXNlX2RhdGU7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLmNvdGUtZmlsbVwiKS5pbm5lclRleHQgPSBkYXRhLnZvdGVfYXZlcmFnZSArIFwiLzEwXCI7XG4gICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLmxhbmd1ZS1vcmlnaW5hbGVcIikuaW5uZXJUZXh0ID0gZGF0YS5vcmlnaW5hbF9sYW5ndWFnZTtcbiAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInAuZHVyZWVcIikuaW5uZXJUZXh0ID0gZGF0YS5ydW50aW1lICsgXCIgbWludXRlc1wiO1xuXG4gICAgICAgIGlmIChkYXRhLmJ1ZGdldCA9PSBcIjBcIikge1xuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInAuYnVkZ2V0XCIpLmlubmVyVGV4dCA9IFwiTm9uIHB1YmxpY1wiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcInAuYnVkZ2V0XCIpLmlubmVyVGV4dCA9IFwiIFVTRCQgXCIgKyBkYXRhLmJ1ZGdldDtcbiAgICAgICAgfVxuXG4gICAgICAgIGlmIChkYXRhLnJldmVudWUgPT0gXCIwXCIpIHtcbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCJwLnJlY2V0dGVcIikuaW5uZXJUZXh0ID0gXCJOb24gcHVibGljXCI7XG4gICAgICAgIH0gZWxzZSB7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiIFVTRCQgXCIgKyBkYXRhLnJldmVudWU7XG4gICAgICAgIH1cblxuICAgICAgICBpZiAoZGF0YS5vdmVydmlldyA9PSBcIlwiKSB7XG4gICAgICAgICAgICBkb2N1bWVudC5xdWVyeVNlbGVjdG9yKFwiLmRlc2NyaXB0aW9uXCIpLmlubmVyVGV4dCA9IFwiQ2UgZmlsbSBuJ2EgcGFzIGRlIGRlc2NyaXB0aW9uIGVuIGxhbmd1ZSBmcmFuw6dhaXNlLlwiO1xuICAgICAgICB9IGVsc2Uge1xuICAgICAgICAgICAgZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi5kZXNjcmlwdGlvblwiKS5pbm5lclRleHQgPSBkYXRhLm92ZXJ2aWV3O1xuICAgICAgICB9XG4gICAgfVxuXG4gICAgLy8gUsOJQ1VQw4lSQVRJT04gREUgTCdJTkZPUk1BVElPTiBERVMgQUNURVVSU1xuICAgIHJlcXVldGVJbmZvQWN0ZXVycyhpZEZpbG0pIHtcbiAgICAgICAgdmFyIHhociA9IG5ldyBYTUxIdHRwUmVxdWVzdCgpO1xuXG4gICAgICAgIHhoci5hZGRFdmVudExpc3RlbmVyKFwicmVhZHlzdGF0ZWNoYW5nZVwiLCB0aGlzLnJldG91clJlcXVldGVJbmZvc0FjdGV1cnMuYmluZCh0aGlzKSk7XG4gICAgICAgIHhoci5vcGVuKFwiR0VUXCIsIHRoaXMuYmFzZVVSTCArIFwibW92aWUvXCIgKyBpZEZpbG0gKyBcIi9jcmVkaXRzP2FwaV9rZXk9XCIgKyB0aGlzLkFQSUtleSk7XG4gICAgICAgIHhoci5zZW5kKCk7XG4gICAgfVxuXG4gICAgLy8gUkVUT1VSIERFIExBIFJFUVXDilRFIFBPVVIgTCdJTkZPUk1BVElPTiBERVMgQUNURVVSU1xuICAgIHJldG91clJlcXVldGVJbmZvc0FjdGV1cnMoZSkge1xuICAgICAgICBsZXQgdGFyZ2V0ID0gZS5jdXJyZW50VGFyZ2V0O1xuICAgICAgICBsZXQgZGF0YTtcblxuICAgICAgICBpZiAodGFyZ2V0LnJlYWR5U3RhdGUgPT09IHRhcmdldC5ET05FKSB7XG4gICAgICAgICAgICBkYXRhID0gSlNPTi5wYXJzZSh0YXJnZXQucmVzcG9uc2VUZXh0KS5jYXN0O1xuICAgICAgICAgICAgdGhpcy5hZmZpY2hlckluZm9zQWN0ZXVycyhkYXRhKTtcbiAgICAgICAgfVxuICAgIH1cblxuICAgIC8vIEFGRklDSEFHRSBERSBMJ0lORk9STUFUSU9OIERFUyBBQ1RFVVJTXG4gICAgYWZmaWNoZXJJbmZvc0FjdGV1cnMoZGF0YSkge1xuICAgICAgICBmb3IgKGxldCBpID0gMDsgaSA8IHRoaXMudG90YWxBY3RldXI7IGkrKykge1xuICAgICAgICAgICAgbGV0IHVuQWN0ZXVyID0gZG9jdW1lbnQucXVlcnlTZWxlY3RvcihcIi50ZW1wbGF0ZT5hcnRpY2xlLmFjdGV1clwiKS5jbG9uZU5vZGUodHJ1ZSk7XG4gICAgICAgICAgICB1bkFjdGV1ci5xdWVyeVNlbGVjdG9yKFwiaW1nXCIpLnNldEF0dHJpYnV0ZShcInNyY1wiLCB0aGlzLmltZ1BhdGggKyBcIndcIiArIHRoaXMudGFpbGxlQWN0ZXVyc1sxXSArIGRhdGFbaV0ucHJvZmlsZV9wYXRoKTtcbiAgICAgICAgICAgIHVuQWN0ZXVyLnF1ZXJ5U2VsZWN0b3IoXCJoNVwiKS5pbm5lclRleHQgPSBkYXRhW2ldLm5hbWU7XG5cbiAgICAgICAgICAgIGRvY3VtZW50LnF1ZXJ5U2VsZWN0b3IoXCIubGlzdGUtYWN0ZXVyc1wiKS5hcHBlbmRDaGlsZCh1bkFjdGV1cik7XG4gICAgICAgIH1cbiAgICB9XG5cblxufSJdLCJmaWxlIjoic2NyaXB0LmpzIn0=
